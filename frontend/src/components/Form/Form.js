import React from 'react';
import Button from "../Button/Button";

const Form = (props) => {
    return (
        <div>
            <form>
                <textarea cols="55" rows="5" value={props.message} onChange={e => props.setMessage(e.target.value)}  className="form"/>
                <Button type="button" sendRequestAddMessage={props.sendRequestAddMessage}/>
            </form>
        </div>
    );
};

export default Form;