import React from 'react';

const Button = (props) => {
    return (
        <div>
            <button type="button" onClick={props.sendRequestAddMessage} className="button">Send Message</button>
        </div>
    );
};

export default Button;