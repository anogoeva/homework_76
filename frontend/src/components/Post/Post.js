import React, {Component} from 'react';

class Post extends Component {
    render() {
        const {message, author, datetime} = this.props;
        return (
            <article className="Post">
                <div className="messages">
                    <div className="message">Сообщение: <b>{message}</b></div>
                    <div className="message_author">Автор: {author}</div>
                    <div className="message_date">Дата: {datetime}</div>
                </div>
            </article>
        );
    }
}
export default Post;