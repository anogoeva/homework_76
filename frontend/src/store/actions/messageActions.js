import axios from "axios";

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';

export const ADD_MESSAGE_REQUEST = 'ADD_MESSAGE_REQUEST';
export const ADD_MESSAGE_SUCCESS = 'ADD_MESSAGE_SUCCESS';
export const ADD_MESSAGE_FAILURE = 'ADD_MESSAGE_FAILURE';

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = message => ({type: FETCH_MESSAGES_SUCCESS, payload: message});
export const fetchMessagesFailure = error => ({type: FETCH_MESSAGES_FAILURE, payload: error});

export const addMessageRequest = () => ({type: ADD_MESSAGE_REQUEST});
export const addMessageSuccess = message => ({type: ADD_MESSAGE_SUCCESS, payload: message});
export const addMessageFailure = error => ({type: ADD_MESSAGE_FAILURE, payload: error});

export const fetchData = () => {
    return async dispatch => {
        try {
            dispatch(fetchMessagesRequest());
            const response = await axios.get('http://localhost:8000/messages/');
            dispatch(fetchMessagesSuccess(response.data));
        } catch (error) {
            dispatch(fetchMessagesFailure(error));
        }
    }
};

export const addMessage = (message) => {
    return async dispatch => {
        try {
            dispatch(addMessageRequest());
            const response = await axios.post('http://localhost:8000/messages/', message);
            dispatch(addMessageSuccess(response.data));
        } catch (error) {
            dispatch(addMessageFailure(error));
        }
    }
};

export const fetchDataByDate = (date) => {
    return async dispatch => {
        try {
            const response = await axios.get('http://localhost:8000/messages?datetime=' + date);
            if (response.data.length !== 0) {
                dispatch(fetchData());
            }
        } catch (error) {
            console.log(error);
        }
    }
};
