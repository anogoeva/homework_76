import {
    ADD_MESSAGE_FAILURE,
    ADD_MESSAGE_REQUEST,
    ADD_MESSAGE_SUCCESS,
    FETCH_MESSAGES_FAILURE,
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS
} from "../actions/messageActions";


const initialState = {
    loading: false,
    error: null,
    messages: null,
    dateTimeLastMessage: null,
    keyForMessage: null
};

const messageReducer = (state = initialState, action) => {
    switch (action.type) {

        case FETCH_MESSAGES_REQUEST:
            return {...state, loading: true};
        case FETCH_MESSAGES_SUCCESS:
            return {
                ...state,
                loading: false,
                messages: action.payload,
                dateTimeLastMessage: action.payload[action.payload.length - 1].dateTime,
                keyForMessage: action.payload.length - 1
            };
        case FETCH_MESSAGES_FAILURE:
            return {...state, loading: false, error: action.payload};

        case ADD_MESSAGE_REQUEST:
            return {...state, loading: true};
        case ADD_MESSAGE_SUCCESS:
            const obj = {};
            obj[state.keyForMessage + 1] = action.payload;
            return {
                ...state,
                loading: false,
                keyForMessage: state.keyForMessage + 1,
                dateTimeLastMessage: action.payload.dateTime,
                messages: {
                    ...state.messages,
                    ...obj
                }
            };
        case ADD_MESSAGE_FAILURE:
            return {...state, loading: false, error: action.payload};

        default:
            return state;
    }
};

export default messageReducer;