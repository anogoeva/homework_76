import './App.css';
import {useEffect, useState} from "react";
import Post from "./components/Post/Post";
import Form from "./components/Form/Form";
import {useDispatch, useSelector} from "react-redux";
import {addMessage, fetchData, fetchDataByDate} from "./store/actions/messageActions";
import Spinner from "./components/Spinner/Spinner";

const App = () => {

    const dispatch = useDispatch();
    const messages = useSelector(state => state.message.messages);
    const loading = useSelector(state => state.message.loading);
    const dateTime = useSelector(state => state.message.dateTimeLastMessage);

    const [message, setMessage] = useState('');

    useEffect(() => {
        dispatch(fetchData());
    }, [dispatch]);

    useEffect(() => {
        const interval = setInterval(() => {
            const fetchData = async () => {
                if (dateTime !== null) {
                    dispatch(fetchDataByDate(dateTime));
                }
            };
            fetchData().catch(e => console.error(e));
        }, 2000);
        return () => clearInterval(interval);
    }, [messages, dateTime, dispatch]);

    const sendRequestAddMessage = () => {
        const data = {message: message, author: "Aijan Nogoeva"};
        dispatch(addMessage(data));
        setMessage('');
    }

    let form = (
        <div className="container">
            <div className="add_message">
                <Form message={message} setMessage={setMessage} sendRequestAddMessage={sendRequestAddMessage}/>
            </div>
            <br/>
            <div className="all_messages">

                {messages && Object.entries(messages).reverse().map(([key, value], i) => {
                    return (
                        <Post
                            key={key}
                            message={value.message}
                            author={value.author}
                            datetime={value.dateTime}
                        />
                    );
                })}
            </div>
        </div>
    );

    if (loading) {
        form = <Spinner/>
    }

    return (
        <div className="App">
            {form}
        </div>
    )
};
export default App;
