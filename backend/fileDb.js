const fs = require('fs');
const {nanoid} = require('nanoid');
const dayjs = require('dayjs');

const filename = './db.json';
let data = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readFileSync(filename);
      data = JSON.parse(fileContents);
    } catch (e) {
      data = [];
    }
  },
  getItems() {
    if (data.length <= 30) {
      return data;
    } else {
      return data.slice(Math.max(data.length - 30, 0));
    }
  },
  getItem(date) {
    const sentData = [];
    data.map(value => {
      if (date < new Date(value.dateTime)) {
        sentData.push(value);
        return sentData;
      } else {
        return sentData;
      }
    });
    return sentData;
  },
  addItem(item) {
    item.id = nanoid();
    item.dateTime = dayjs().format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    data.push(item);
    this.save();
    return item;
  },
  save() {
    fs.writeFileSync(filename, JSON.stringify(data));
  }
};