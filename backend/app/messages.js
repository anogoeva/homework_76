const express = require('express');
const fileDb = require('../fileDb');
const router = express.Router();


router.post('/', (req, res) => {
    const author = req.body.author;
    const message = req.body.message;

    if (!author || !message) {
        res.status(400).send({error: 'Author and message must be present in the request'});
    } else {
        const addNewMessage = fileDb.addItem({
            author: req.body.author,
            message: req.body.message
        });
        res.send(addNewMessage);
    }
});


router.get('/', (req, res) => {
    if (req.query.datetime) {
        const dateFromQuery = req.query.datetime;
        const dateParse = new Date(dateFromQuery);

        if (isNaN(dateParse.getDate())) {
            res.status(400).send('Date is wrong. Please, check date format.')
        } else {
            res.send(fileDb.getItem(dateParse));
        }

    } else {
        const messages = fileDb.getItems();
        res.send(messages);
    }
});
module.exports = router; // export default router;